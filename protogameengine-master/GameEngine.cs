﻿using System;
namespace GameTools
{

    /*
     * GameEngine:
     * Protype to print sequence of frames in console.
     * 
     * To stop de main while press ESC key.
     * 
     */

    public class GameEngine
    {

        //Declaració d'una variable
        private ConsoleColor _backgroundConsoleColor;
        //Declaració d'una propietat
        public ConsoleColor BackgroundConsoleColor
        {
            get { return _backgroundConsoleColor; }
            set
            {
                // #### Si el color de fons es diferent a blanc o negre = set el nou color de fons.
                // Sino = set el color dels fons a gris i tira una excepció.
                if (ConsoleColor.White != value || ConsoleColor.Black != value) _backgroundConsoleColor = value;
                else
                {
                    _backgroundConsoleColor = ConsoleColor.Gray;
                    throw new ArgumentException($"Console color {value} not recomended. Set by default");
                }
            }
        }

        private int time2liveframe;
        private float _frameRate;
        public float FrameRate
        {
            get { return _frameRate; }
            set
            {
                //Ternari condition
                // #### Si el nou valor que es vol colocar es menor que 0 el torna positiu abans de canviar-lo.
                _frameRate = (value < 0f) ? value * (-1f) : value;
            }
        }


        //Declaració d'una propietat no protegida
        public int Frames { get; set; }

        private ConsoleKeyInfo cki;
        private bool engineSwitch;

        public GameEngine()
        {
            InitGame();
            UpdateGame();
            CloseGame();
        }


        private void InitGame()
        {
            /*** Init variables ***/

            Frames = 0;
            engineSwitch = false;

            //Acces exemple with this:
            this._frameRate = (_frameRate <= 0) ? 12 : _frameRate;

            //Calculate the frame time in miliseconds. Time to refresh. F=1/s ->s=1/F
            time2liveframe = (int)((1 / _frameRate) * 1000);

            /*******/

            //Prepare Console
            CleanFrame();
            Console.BackgroundColor = _backgroundConsoleColor;

            Console.WriteLine("\nPress a key to display; press the ESC key to quit.");

            Console.WriteLine($"Game Initiation             Render data: Framerate: {_frameRate} || TimeToRefresh:{time2liveframe}");

            Start();
           
            System.Threading.Thread.Sleep(2000);
        }

        /*
         * Engine updates every frame
         * 
         * Reprint console
         */
        private void UpdateGame()
        {
            // #### 
            do
            {

                while (Console.KeyAvailable == false)
                {
                   
                    CleanFrame();

                    Update();
                    
                    CheckKeyboard4Engine();

                    Console.WriteLine(engineSwitch); // ### Escriu False de forma infinita

                    RefreshFrame();

                    Frames++;
                }

                cki = Console.ReadKey(true);

            } while (engineSwitch);

        }

        private void ListenKeyboard()
        {
            cki = Console.ReadKey();
        }

        private void CheckKeyboard4Engine()
        {
            engineSwitch = (cki.Key == ConsoleKey.Escape);
        }

        private void RefreshFrame()
        {
            //Access to Threading library only in this line
            System.Threading.Thread.Sleep(time2liveframe);
        }

        private void CloseGame()
        {

            Console.WriteLine("You pressed the '{0}' key.", cki.Key);
            Exit(); //----------------------- Està buit.
            Console.WriteLine(" Game Over. Closing game");
        }


        private void CleanFrame() 
        {
            // #### Neteja la pantalla.
            Console.Clear();
            Console.SetCursorPosition(Console.CursorLeft, Console.CursorTop+1);
        }

        protected virtual void Start()
        {
            //Code before first frame
        }

        protected virtual void Update()
        {
            //Execution ontime secuence of every frame
        }

        protected virtual void Exit()
        {
            //Code afer last frame
        }

    }

    public class OverrideGameEngine : GameEngine
    {
        private string[,] array = new string[25, 15];
        // #### FreeSpace mostra quan una columna està lliure per poder fer caure una lletra.
        //      Cada lletra va seguida de 5 numeros, la fila no es pot trencar per posar una altra lletra.
        //      Després de cada fila es deixará un minim de 5 espais.
        private int[] freeSpace = new int[] {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};

        protected override void Start()
        {
            // Crear array de '0'
            for (int i = 0; i < array.GetLength(0); i++)
            {
                for(int j = 0; j  < array.GetLength(1); j++)
                {
                    array[i, j] = " ";
                }
            }
        }

        protected override void Update()
        {
            // #### Crea pluja de caracters.
            //       Una lletra aleatori caurà en cada frame una fila fins arribar al final.
            //       La columna on es generi també serà aleatori.
            int indexLetter;
            bool lletraColocada = false;
            
            /*for (int j = 0; j < freeSpace.Length; j++)
            {
                freeSpace[j]--; // #### Resta un espai a la pluja per asegurar que hi ha cert espai cada espai entre les "gotes" d'una fila.
            }*/
            

            // Fer caure les lletres.
            //
            // X   1   2   3   0   0
            // 0   X   1   2   3   0
            // 0   0   X   1   2   3
            // 0   0   0   X   1   2
            // 0   0   0   0   X   1



            for (int i = array.GetLength(0) - 1; i >= 0; i--)
            {
                for (int j = array.GetLength(1) - 1; j >= 0; j--)
                {
                    if (i != 0)
                    {

                        if (Convert.ToChar(array[i - 1, j]) > Convert.ToChar(64) && Convert.ToChar(array[i - 1, j]) < Convert.ToChar(92)) // Rang ascii per a les lletres.
                        {
                            array[i, j] = RandomLetter(); // Si adalt hi havia una lletra, coloca una lletra.
                        }
                        else if (Convert.ToChar(array[i, j]) > Convert.ToChar(64) && Convert.ToChar(array[i, j]) < Convert.ToChar(92))
                        {
                            array[i, j] = Convert.ToString(RandomNum()); // Si hi ha una lletra, reescriu amb un numero.
                        }
                        else if (i != 1)
                        {
                            if (Convert.ToChar(array[i - 1, j]) > Convert.ToChar(47) && Convert.ToChar(array[i - 1, j]) < Convert.ToChar(58)) // Rang ascii per als numeros.
                            {
                                array[i, j] = Convert.ToString(RandomNum()); // Si adalt hi ha un numero, reescriu amb numero.
                            }
                            else array[i, j] = " ";
                        }
                        else
                        {
                            if (freeSpace[j] > 4) array[i, j] = Convert.ToString(RandomNum());
                            else array[i, j] = " ";
                        }
                      
                        
                    }

                    if (i == 0)
                    {
                        if (Convert.ToChar(array[i, j]) > Convert.ToChar(64) && Convert.ToChar(array[i, j]) < Convert.ToChar(92))
                        {
                            array[i, j] = Convert.ToString(RandomNum());
                        }
                        else if (freeSpace[j] > 5)
                        {
                            array[i, j] = Convert.ToString(RandomNum());
                        }
                        else
                        {
                            array[i, j] = " ";
                        }
                    }
                }
            }

            // Colocar lletra aleatoria a posició aleatoria.
            do
            {
                indexLetter = RandomNumIndex();
                if (freeSpace[indexLetter] <= 0)
                {
                    array[0, indexLetter] = RandomLetter();
                    freeSpace[indexLetter] = 9;
                    lletraColocada = true;
                }

            } while (!lletraColocada);
            
            for (int i = 0; i < freeSpace.Length; i++)
            {
                freeSpace[i]--; 
            }
            
            // #### Mostra per pantalla l'array. 
            for (int i = 0; i < array.GetLength(0); i++)
            {
                for (int j = 0; j < array.GetLength(1); j++)
                {
                    if (Convert.ToChar(array[i, j]) > Convert.ToChar(64) && Convert.ToChar(array[i, j]) < Convert.ToChar(92)) Console.ForegroundColor = ConsoleColor.White;
                    else Console.ForegroundColor = ConsoleColor.Green;
                    Console.Write(array[i, j]);

                    
                }
                Console.WriteLine();
            }
         }

        private int RandomNumIndex()
        {
            Random rnd = new Random();
            return rnd.Next(0,array.GetLength(1));
        }

        private int RandomNum()
        {
            Random rnd = new Random();
            return rnd.Next(1, 9);
        }

        private string RandomLetter()
        {
            Random rnd = new Random();
            char letter = Convert.ToChar(rnd.Next(65,91));
            return Convert.ToString(letter);
        } 


        protected override void Exit()
        {

        }
    }

    public class ConsoleSpiner
    {
        int counter;
        public ConsoleSpiner()
        {
            counter = 0;
        }
        public void Turn()
        {
            counter++;
            switch (counter % 4)
            {
                case 0: Console.Write("/"); break;
                case 1: Console.Write("-"); break;
                case 2: Console.Write("\\"); break;
                case 3: Console.Write("|"); break;
            }
            //Console.SetCursorPosition(Console.CursorLeft - 1, Console.CursorTop);
        }
    }
}





